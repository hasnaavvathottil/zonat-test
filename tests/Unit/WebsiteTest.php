<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Website;
use Illuminate\Foundation\Testing\WithFaker;

class WebsiteTest extends TestCase
{
	use WithFaker;
    /**
     * A  unit test for create website.
     *
     */
    public function testCreateWebsiteSuccessfully()
    {
    	$data = [
    		'name' => $this->faker->sentence(3, true) ,
    		'url' => $this->faker->url,
    		'notes' => $this->faker->paragraph(3, true) ,
    	];
    	$this->json('POST', '/api/websites',$data)
	    	->assertStatus(201)
	    	->assertJson($data)
	    	->assertJsonStructure([
	    		'id',
	    		'name',
	    		'url',
	    		'notes',
	    		'created_at',
	    		'updated_at',
	    	]);
    }
    /**
     * A  unit test for required field in website.
     *
     */
    public function testRequiredFieldForWebsiteCreate()
    {
    	$this->json('POST', '/api/websites',[])
    		->assertStatus(422)
    		->assertJsonValidationErrors(['name','url']);
    }
    /**
     * A  unit test for list website.
     *
     */
    public function testWebsiteListedSuccessfully()
    {
		$web = factory(Website::class)->create();
    	$this->json('GET', '/api/websites')
    		->assertStatus(200)
    		->assertJsonStructure([
	    		[
					'id',
					'name',
					'url',
					'notes',
					'created_at',
					'updated_at'
				]
			]);
    }
    /**
     * A  unit test for update website.
     *
     */
    public function testWebsiteUpdatedSuccessfully()
    {
		$web = factory(Website::class)->create();
		$data = [
    		'name' => $this->faker->sentence(1, true) ,
    		'url' => $this->faker->url,
    		'notes' => $this->faker->paragraph(3, true) ,
    	];
    	$this->json('PATCH', '/api/websites/'.$web->id,$data)
    		->assertStatus(200)
            ->assertJson($data)
            ->assertJsonStructure([
					'id',
					'name',
					'url',
					'notes',
					'created_at',
					'updated_at'
			]);
    }
    /**
     * A  unit test for create website.
     *
     */
    public function testRequiredFieldForWebsiteUpdate()
    {
    	$web = factory(Website::class)->create();
    	$this->json('PATCH', '/api/websites/'.$web->id,[])
    		->assertStatus(422)
	    	->assertJsonValidationErrors(['name','url']);
    }
    /**
     * A  unit test for retrieve website.
     *
     */
    public function testRetrieveWebsiteSuccessfully()
    {
		$web = factory(Website::class)->create();
		
    	$this->json('GET', '/api/websites/'.$web->id)
    		->assertStatus(200)
            ->assertJson($web->toArray())
            ->assertJsonStructure([
					'id',
					'name',
					'url',
					'notes',
					'created_at',
					'updated_at'
			]);
    }
    /**
     * A  unit test for delete website.
     *
     */
    public function testDeleteWebsite()
    {
		$web = factory(Website::class)->create();

    	$this->json('DELETE', 'api/websites/' . $web->id)
    	->assertStatus(204);
    }

}
