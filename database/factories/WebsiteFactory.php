<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Website;
use Faker\Generator as Faker;

$factory->define(Website::class, function (Faker $faker) {
    return [
    	'name' => $faker->sentence(1, true) ,
        'url' => $faker->url,
        'notes' => $faker->paragraph(3, true) ,
    ];
});
